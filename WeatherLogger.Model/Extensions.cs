﻿using ForecastIO;
using ForecastIO.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherLogger.Model
{
    public static class Extensions
    {
        public static WeatherData ToWeatherData( this Currently currently )
        {
            return new WeatherData
            {
                ApparentTemperature = currently.apparentTemperature,
                CloudCover = currently.cloudCover,
                DewPoint = currently.dewPoint,
                Humidity = currently.humidity,
                IconDescription = currently.icon,
                NearestStormBearing = currently.nearestStormBearing,
                NearestStormDistance = currently.nearestStormDistance,
                Ozone = currently.ozone,
                PrecipitationIntensity = currently.precipIntensity,
                PrecipitationProbability = currently.precipProbability,
                Pressure = currently.pressure,
                Summary = currently.summary,
                Temperature = currently.temperature,
                Time = currently.time.ToDateTime(),
                Visibility = currently.visibility,
                WindBearing = currently.windBearing,
                WindSpeed = currently.windSpeed
            };
        }
    }
}
