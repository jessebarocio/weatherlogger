﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherLogger.Model
{
    public class WeatherData
    {
        public DateTime Time { get; set; }
        public string Summary { get; set; }
        public string IconDescription { get; set; }
        public float NearestStormBearing { get; set; }
        public float NearestStormDistance { get; set; }
        public float PrecipitationIntensity { get; set; }
        public float PrecipitationProbability { get; set; }
        public float Temperature { get; set; }
        public float ApparentTemperature { get; set; }
        public float DewPoint { get; set; }
        public float WindSpeed { get; set; }
        public float WindBearing { get; set; }
        public float CloudCover { get; set; }
        public float Humidity { get; set; }
        public float Pressure { get; set; }
        public float Visibility { get; set; }
        public float Ozone { get; set; }
    }
}
