﻿using ForecastIO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherLogger.Model;

namespace WeatherLogger
{
    class Program
    {
        static void Main( string[] args )
        {
            // Read in configuration
            var config = JsonConvert.DeserializeObject<Config>( File.ReadAllText( "Config.json" ) );
            // Read in locations
            var locations = JsonConvert.DeserializeObject<IEnumerable<Location>>( File.ReadAllText( "Locations.json" ) );

            // Iterate through the locations and write the data to the file
            foreach ( var item in locations )
            {
                string fileName = String.Format( "{0}_{1}.json", item.LocationName, DateTime.Today.ToString( "yyyyMMdd" ) );
                string filePath = Path.Combine( item.OutputFolder, fileName );

                JArray data;
                if ( File.Exists( filePath ) )
                {
                    data = (JArray)JArray.ReadFrom( new JsonTextReader( new StringReader( File.ReadAllText( filePath ) ) ) );
                }
                else
                {
                    data = new JArray();
                }

                var forecast = new ForecastIORequest( config.ForecastIOApiKey, item.Latitude, item.Longitude, Unit.us );
                var result = forecast.Get();

                data.Add( JToken.FromObject( result.currently.ToWeatherData() ) );

                File.WriteAllText( filePath, data.ToString( Formatting.None ) );
            }
        }
    }
}
